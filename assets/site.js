const el = document.getElementById.bind(document);
const query = document.querySelectorAll.bind(document);

var on_ready;

function on_content_loaded() {
	query('.js-required').forEach(item => item.classList.remove('js-required'));

	if (on_ready) on_ready();
}

if (document.readyState === 'loading') {
	document.addEventListener('readystatechange', (event) => {
		if (document.readyState === 'interactive')
			on_content_loaded();
	});
} else {
	on_content_loaded();
}
