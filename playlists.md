---
---
## Playlists

- Current playlist:
  - [Scomber - Broadway](http://ccmixter.org/files/scomber/62146) - 4:13 -
    [CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/).
  - [Monplaisir - Stage 1 Level 24](https://freemusicarchive.org/music/Monplaisir/Heat_of_the_Summer/Monplaisir_-_Monplaisir_-_Heat_of_the_Summer_-_04_Stage_1_Level_24) - 5:19 -
    [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
  - [Kriss - Jazz Club](https://freemusicarchive.org/music/Kriss/nomad_ep/unfound38_03_-_kriss_-_jazz_club) - 5:25 -
    [CC BY-ND 3.0](https://creativecommons.org/licenses/by-nd/3.0/).
  - [Nobara Hayakawa - Trail](https://freemusicarchive.org/music/Nobara_Hayakawa/Trail_EP/Nobara_Hayakawa_-_Trail_EP_-_Trail) - 3:26 -
    [CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/).

