---
---
## About

<figure class="img">
  <img src="/assets/breakfast.jpg" alt="">
  <figcaption>
    Welcome to the Not Café
  </figcaption>
</figure>

Welcome, take a seat, turn on the radio, relax. Today we'll talk about technology,
programming, open-source and science. Welcome to The Not Café, a blog dedicated to
technology. There will be tutorials, news, project announcements, reviews and everything
else we enjoy. The Not Café - Enjoy Technology.

### Donate
Enjoying the stay ? Consider [donating](/donate) to The Not Café.

### News feed
The [Atom](https://en.wikipedia.org/wiki/Atom_(Web_standard)) news feed file is available
at [feed.xml](/feed.xml) for usage with news aggregator software.

### Site content license
Content in this site is copyrighted material by The Not Café and licensed under the
Creative Commons Attribution-ShareAlike license
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). The site source code
is available at [Gitlab](https://gitlab.com/hdante/not-cafe/-/tree/master).

### Social Media
- Twitter: [@thenotcafe](https://twitter.com/thenotcafe)
- Mastodon: [@notcafe@mastodon.technology](https://mastodon.technology/@notcafe)
- Facebook: [thenotcafe](https://www.facebook.com/thenotcafe)

### Playlists
Current playlist:

- [Scomber - Broadway](http://ccmixter.org/files/scomber/62146) - 4:13 -
  [CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/).
- [Monplaisir - Stage 1 Level 24](https://freemusicarchive.org/music/Monplaisir/Heat_of_the_Summer/Monplaisir_-_Monplaisir_-_Heat_of_the_Summer_-_04_Stage_1_Level_24) - 5:19 -
  [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
- [Kriss - Jazz Club](https://freemusicarchive.org/music/Kriss/nomad_ep/unfound38_03_-_kriss_-_jazz_club) - 5:25 -
  [CC BY-ND 3.0](https://creativecommons.org/licenses/by-nd/3.0/).
- [Nobara Hayakawa - Trail](https://freemusicarchive.org/music/Nobara_Hayakawa/Trail_EP/Nobara_Hayakawa_-_Trail_EP_-_Trail) - 3:26 -
  [CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/).

See also [archived playlists](/playlists).

### Assets and credits
- Original site theme: [466 Cafe House](https://templatemo.com/tm-466-cafe-house) from
  [TemplateMo](https://templatemo.com/) - free for commercial or non-commercial sites.
- Original coffee cup logo image:
  [Coffe cup drink hot drink](https://pixabay.com/vectors/coffee-cup-drink-hot-drink-34251/)
  image by
  [Clker-Free-Vector-Images](https://pixabay.com/users/Clker-Free-Vector-Images-3736/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=34251)
  from
  [Pixabay](https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=34251) -
  [Pixabay License](https://pixabay.com/service/license/).
- Donation page coffee with heart art: [Heart latte art](https://www.pikrepo.com/fpjyt/heart-latte-art) from [Pikrepo](https://www.pikrepo.com/) - free for commercial use.
- Site fonts: [Roboto](https://fonts.google.com/specimen/Roboto),
  [Damion](https://fonts.google.com/specimen/Damion) and
  [Inconsolata](https://fonts.google.com/specimen/Inconsolata) from
  [Google Fonts](https://fonts.google.com/) -
  [Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0) and
  [Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).
- About page breakfast picture: [614378]( https://pxhere.com/en/photo/614378) from
  [PxHere](https://pxhere.com/) - public domain.
- Mastodon icon: [mastodon](https://fontawesome.com/icons/mastodon?style=brands) from
  [Font Awesome](https://fontawesome.com/) -
  [CC BY 4.0](https://fontawesome.com/license/free).
- Twitter icon:
  [Social icons](https://about.twitter.com/content/dam/about-twitter/company/brand-resources/en_us/Twitter-Social-Icons.zip) from
  [Twitter Brand Resources](https://about.twitter.com/en_us/company/brand-resources.html) -
  [Twitter Trademark Guidelines](https://about.twitter.com/content/dam/about-twitter/company/brand-resources/en_us/Twitter_Brand_Guidelines_V2_0.pdf) -
  TWITTER, TWEET, RETWEET and the Twitter logo are trademarks of Twitter, Inc. or its
  affiliates.
- WhatsApp icon: [White and Green](https://whatsappbrand.com/) from
  [WhatsApp Brand Resources](https://whatsappbrand.com/) -
  [WhatsApp Brand Guidelines](https://whatsappbrand.com/wp-content/themes/whatsapp-brc/downloads/WhatsApp-Brand-Guidelines-Optimized.pdf) - The WhatsApp name and logos are trademarks of
  WhatsApp.
- e-mail icon: [Tokyoship Mail icon](https://commons.wikimedia.org/wiki/File:Tokyoship_Mail_icon.svg) by [Tokyoship](https://commons.wikimedia.org/wiki/User:Tokyoship)
  from [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) -
  [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
- Facebook icon: ["f" Logo](https://en.facebookbrand.com/facebookapp/assets/f-logo?audience=landing) from [Facebook Brand Resource Center](https://en.facebookbrand.com/) - [Facebook Brand Assets Guide](https://en.facebookbrand.com/facebookapp/)
- AV1 logo:
  [AOMedia: Logo Files](http://downloads.aomedia.org/assets/zip/Final_Logo_1.16.18.zip)
  from [AOMedia Press Kit](http://aomedia.org/news/aomedia-press-kit/) - public domain.

### Privacy information
This site does not collect any personal information, not even IP address and access times,
does not use cookies or any kind of local storage and does not forward any information
to third parties. It should be fully compatible with modern data protection legislation,
like GDPR, CCPA and LGPD (but I'm not a lawyer).

The hosting provider of this site ([Netlify](https://netlify.com)) might collect personal
information, like IP addresses for logging purposes, but they're inaccessible to this
site's administrators. The collection method is compliant with GDPR and CCPA. For more
information, see: [Netlify: GDPR-CCPA](https://www.netlify.com/gdpr-ccpa/).

We reserve the right to change this policy in the future if we decide it's necessary to do
so.
